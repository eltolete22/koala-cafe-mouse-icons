Howdy there!

This gitlab page contains PNG's that you can use to replace the default ROBLOX mouse textures with!
The PNG's in the "KeyboardMouse" are all koala/Koala Cafe related!

**WARNING: THE RIGHTS FOR 1 OF THE IMAGES BELONGS TO KOALA ASSOCIATION. ALL CREDIT GOES TO THEM. THE PNG IN QUESTION IS THE ONE THAT HAS "KOALA CAFE" AS TEXT.**

                            **INSTALLATION INSTRUCTIONS***

                            Assuming you already downloaded this...
                            Using the script: Open the terminal where your script is. In windows go to the folder
                            the script is in, right click, and click "open in terminal"
                            
                            You can do this manually as well.
                            Sharkblox has a decent guide on how to do
                            it: https://www.youtube.com/watch?v=VsgiJQQAIZ0

Q: How did you edit these images?

A: The software I used to do this was GIMP. First, I opened the original textures in GIMP. then, I added the custom images as a seperate layer, downscaled the images, and removed the background on one. For one I just put text.

Q: Help! This script doesn't work!

A: Wait for me to update the repository. You see, the script depends on a folder that gets it's name changed every ROBLOX update.
Give me a few hours to a day or 2 and I will update the script. If you can't wait just do it manually.

Q: Help! The icons reset!

A: Because of how ROBLOX updates work, the textures get reset/redownloaded every time you update. In this case run the installation script again.

